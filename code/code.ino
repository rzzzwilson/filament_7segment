//**********************************************************************
// Test code for the 7 segment filament display.
//**********************************************************************

#include "filament.h"
#include "display.h"
#include "utility.h"


//********************************************************
// Initialize everything.
//********************************************************

void setup()
{
 // start serial 
  Serial.begin(115200);

  // initialize the display, set brightness, face, show boot status
  disp_begin();
  disp_brightness(0);

  // clear the display, ready to go
  disp_clear();
}

void loop()
{
  static bool dp = false;

  disp_brightness(15);

  for (int val = 0; val < 10; ++val)
  {
    dp = !dp;
  
    disp_num(val, dp);
    delay(250);
  }

  disp_num(8, true);

  for (int b = 0; b < 16; ++b)
  {
    disp_brightness(b);
    delay(100);
  }

  for (int b = 15; b; --b)
  {
    disp_brightness(b);
    delay(100);
  }

  delay(1000);
}
