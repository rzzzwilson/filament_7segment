//********************************************
// Code for the one digit 7 segment filament display.
//********************************************

#include <SPI.h>

#include "filament.h"
#include "display.h"
#include "utility.h"


// SS pin on controller
const int SSPin = D8;

// define 7219 register addresses for setup
const byte DECODE_MODE = 0x09;  // 0x00 = No decode for digits
const byte INTENSITY = 0x0A;    // 0x08 = mid level. Range is 0x00 to 0x0F
const byte SCAN_LIMIT = 0x0B;   // 0x04 for all 4 digits
const byte SHUTDOWN = 0x0C;     // 0x00 - shutdown, 0x01 = normal
const byte DISPLAY_TEST = 0x0F; // 0x00 = normal, 0x01 = display test mode all on full
const byte DIGIT_0 = 0x01;      // address for digit 0
const byte DIGIT_1 = 0x02;      // address for digit 1
const byte DIGIT_2 = 0x03;      // address for digit 2 (upside down)
const byte DIGIT_3 = 0x04;      // address for digit 3 (upside down)

// define bitmasks for the LED segments
const byte LED_DP = 0x80;
const byte LED_SA = 0x40;
const byte LED_SB = 0x20;
const byte LED_SC = 0x10;
const byte LED_SD = 0x08;
const byte LED_SE = 0x04;
const byte LED_SF = 0x02;
const byte LED_SG = 0x01;

// "font" data for encoded symbols
const byte LED_D0 = 0b01111110;     // 0
const byte LED_D1 = 0b00110000;     // 1
const byte LED_D2 = 0b01101101;     // 2
const byte LED_D3 = 0b01111001;     // 3
const byte LED_D4 = 0b00110011;     // 4
const byte LED_D5 = 0b01011011;     // 5
const byte LED_D6 = 0b01011111;     // 6
const byte LED_D7 = 0b01110000;     // 7
const byte LED_D8 = 0b01111111;     // 8
const byte LED_D9 = 0b01111011;     // 9
const byte LED_BLANK = 0b00000000;  // blank
const byte LED_DASH = 0b00000001;   // dash

byte digits[] = {LED_D0, LED_D1, LED_D2, LED_D3, LED_D4,
                 LED_D5, LED_D6, LED_D7, LED_D8, LED_D9,
                 LED_BLANK, LED_DASH};

const byte DIG_BLANK = 10;
const byte DIG_DASH = 11;

//---------------------------------------------
// Send SPI "data" to MAX7219 address "addr".
//---------------------------------------------

static void spi_send(byte addr, byte data)
{
  digitalWrite(SSPin, LOW);
  SPI.transfer(addr);
  SPI.transfer(data);
  digitalWrite(SSPin, HIGH);
}

//---------------------------------------------
// Initialize the display.
//---------------------------------------------

void disp_begin(void)
{
  pinMode(SSPin, OUTPUT);  
  digitalWrite(SSPin, HIGH);

  // turn on SPI port
  SPI.begin();
  spi_send(DISPLAY_TEST, 0x00);   // non-test mode
  spi_send(SHUTDOWN, 0x01);       // display to normal mode
  spi_send(SCAN_LIMIT, 1);        // set display of 1 digit
  spi_send(INTENSITY, 0);         // set minimum intensity
  disp_clear();                   // clear display
}

//---------------------------------------------
// Show the display in TEST mode, everything ON.
//     pause  the time in milliseconds to show the test
//---------------------------------------------

void disp_test(int pause)
{
  spi_send(DISPLAY_TEST, 0x01);   // test mode
  delay(pause);
  spi_send(DISPLAY_TEST, 0x00);   // non-test mode
}

//---------------------------------------------
// Clear the display.
//---------------------------------------------

void disp_clear(void)
{
  spi_send(DIGIT_0, LED_BLANK);
}

//---------------------------------------------
// Set the display brightness.
//     bright  the brightness level [0..8]
//
// Actual setting is "bright" + CONFIG brightness.
//---------------------------------------------

void disp_brightness(int bright)
{
  spi_send(INTENSITY, bright);
}

//---------------------------------------------
// Display a number.
//     num  the number to display
//---------------------------------------------

void disp_num(int num, bool dp)
{
  byte show_dp = (dp) ? LED_DP : 0;
  
  spi_send(DIGIT_0, digits[num] + show_dp);
}
