//********************************************
// Interface to a simple library to handle debug printing.
//********************************************

#ifndef _UTILITY_H_
#define _UTILITY_H_

void printf(char *fmt, ...);

#endif
