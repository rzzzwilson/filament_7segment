//********************************************
// Common stuff for the filament display.
//********************************************

#ifndef _FILAMENT_H_
#define _FILAMENT_H_


#include <Arduino.h>


//--------------------------------------------------------
// Set max/min brightness values
//--------------------------------------------------------

const int MinBrightness = 1;    // min/max brightness settings
const int MaxBrightness = 7;

// macro for comparing strings more naturally
#define STREQ(a, b) (strcmp((a), (b)) == 0)

// macro to turn off warnings about "unused parameter" in functions
#define UNUSED(v) (void) (v)


#endif
