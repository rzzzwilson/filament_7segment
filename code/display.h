//********************************************
// Interface to a simple library to handle the 8x8 WS2812B display.
// Same-ish API as the SmartClock API.
//********************************************

#ifndef _DISPLAY_H_
#define _DISPLAY_H_


void disp_begin(void);
void disp_clear(void);
void disp_num(int num, bool dp);
void disp_brightness(int level);
void disp_test(int msec);
void disp_debug(int num);


#endif
