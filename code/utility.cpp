//********************************************
// Interface to simple debug printing functions.
//********************************************

#include "filament.h"
#include "utility.h"


//--------------------------------------------------------
// Provide a "printf()" function, shortens code.
// Always prints.
//--------------------------------------------------------

void printf(char *fmt, ...)
{
  va_list ptr;
  char tmp_buff[512];

  memset(tmp_buff, 0, sizeof(tmp_buff));
  
  va_start(ptr, fmt);
  vsprintf(tmp_buff,fmt, ptr);
  va_end(ptr);

  Serial.print(tmp_buff);
}
